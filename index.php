<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Classes and objects</title>
</head>
<body>
	<h1>Objects from Variable</h1>

	<p><?php echo $buildingObj->name; ?></p>

	<h1>Objects from classes</h1>

	<p><?php var_dump($building) ?></p>

	<h1>Inheritance (Condominium Object)</h1>

	<p><?php echo $condominium->name; ?></p>

	<p><?php echo $condominium->floors; ?></p>

	<h1>Polymorphism (Changing the printName Behavior)</h1>

	<p><?php echo $condominium->printName(); ?></p>

</body>
</html>
